var express = require('express');
var bodyParser = require('body-parser');
var dyt = require('./dyt');
var rant = require('./rant');

var app = express();
var port = process.env.PORT || 3000;

// body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));

// test route
app.get('/', function (req, res) { res.status(200).send('What can i do for you, Sir?') });

// post route
app.post('/dyt', dyt);
app.post('/rant', rant);

// error handler
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(400).send(err.message);
});

app.listen(port, function () {
  console.log('Slack bot listening on port ' + port);
});